export default {
  state: {
    breadCrumbList: [],
    menuListData: [],
    homeRoute: {},
    juyouTicket: {},
    navIndex: 0,
    navBarShow: false
  },
  getters: {
    getBreadCrumbList: state => state.breadCrumbList
  },
  mutations: {
    setBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    appendBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    setMenuListData (state, data) {
      state.menuListData = data
    },
    setJuyouTicket (state, data) {
      state.juyouTicket = data
    },
    setNavIndex (state, data) {
      state.navIndex = data
    },
    setNavBarShow (state, data) {
      state.navBarShow = data
    }
  },
  actions: {
    setTicket ({ commit }, data) {
      commit('setJuyouTicket', data)
    },
    setNavIndex ({ commit }, data) {
      commit('setNavIndex', data)
    },
    setNavBarShow ({ commit }, data) {
      commit('setNavBarShow', data)
    }
  }
}
