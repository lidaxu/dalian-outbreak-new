import { setToken, getToken } from '@/libs/util'
export default {
  state: {
    severUserInfo: {},
    token: getToken()
  },
  getters: {
    userInfo: state => state.severUserInfo
  },
  mutations: {
    setSeverUserInfo (state, severUserInfo) {
      state.severUserInfo = severUserInfo
    },
    setToken (state, token) {
      state.token = token
      setToken(token)
    }
  },
  actions: {
    userLogin ({ commit }, { userName, password }) {
      return new Promise((resolve, reject) => {
        userLogin().then(ret => {
          const data = ret.data
          if (data.status === 1201) {
            commit('setToken', '3334455667788')
          }
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}
