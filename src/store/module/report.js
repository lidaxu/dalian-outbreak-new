export default {
  state: {
    form: {}
  },
  getters: {},
  mutations: {
    SET_FORM: (state, form) => {
      state.form = form
    }
  },
  actions: {
    setForm({ commit }, res) {
      commit('SET_FORM', res)
    }
  }
}
