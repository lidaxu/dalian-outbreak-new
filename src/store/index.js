import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'
import report from './module/report'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      storage: window.sessionStorage
    })
  ],
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    app,
    report
  }
})
