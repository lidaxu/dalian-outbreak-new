import xeUtils from 'xe-utils'
export default {
   /**
      * @description 配置显示在浏览器标签的title
      */
   title: '慧鼎2019基础框架',
   /**
      * @description token在Cookie中存储的天数，默认1天
      */
   cookieExpires: 1,
   /**
      * @description 是否使用国际化，默认为false
      *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
      *              用来在菜单中显示文字
      */
   debug: xeUtils.locat().host !== 'shengst.tripln.top' && xeUtils.locat().host !== 'sst.wanglanglang.com',
   company_code: xeUtils.locat().host !== 'shengst.tripln.top' ? 'l0002484b91af51' : 'tz00413874f56d3d2',
   testip: xeUtils.locat().host !== 'shengst.tripln.top' ? 'true' : 'false',
   /**
      * @description api请求基础路径
      */
   baseUrl: xeUtils.locat().origin,
   baseUrlMZ: xeUtils.locat().host !== 'shengst.tripln.top' ? 'http://iview.tripln.top' : 'https://qstb.tripln.top',
   baseUrlShop: xeUtils.locat().host == 'localhost:8081' ? '' : 'https://mobile.tripln.top',
   baseUrlForecast: xeUtils.locat().host == 'localhost:8081' ? 'http://124.95.134.20' : 'http://124.95.134.20',
   baseUrlLD: xeUtils.locat().host = 'http://symjld.lngov.top',
   publicPath: {
      dev: '',
      //  pro: xeUtils.locat().protocol == 'https:' ? 'https://dalianoutbreaknew.lngov.top' : 'http://dalianoutbreaknew.lngov.top'
      pro: xeUtils.locat().protocol == 'https:' ? 'https://59.46.238.106:38012' : 'http://59.46.238.106:38012'

      // pro: xeUtils.locat().protocol == 'https:' ? 'https://beta.tripln.com/scp-transportationdalian/' : 'http://beta.tripln.com/scp-transportationdalian'

      // pro: 'https://iview.tripln.top/'// 民政的测试域名1
      // pro: 'http://allin.tripln.top/'// 民政的测试域名
      // pro: 'http://view.tripln.top/' // 移动端的测试域名
      // pro: 'http://192.168.0.25:38985/'
   },
   /**
      * @description 默认打开的首页的路由name值，默认为home
      */
   homeName: 'login',
   /**
      * @description 需要加载的插件
      */
   plugin: {
      'error-store': {
         showInHeader: true, // 设为false后不会在顶部显示错误日志徽标
         developmentOff: true // 设为true后在开发环境不会收集错误信息，方便开发中排查错误
      }
   }
}
