export default {
  errcode: 200,
  errmsg: 'ok',
  data: [
    {
      value: 'shq',
      label: '沈河区',
      children: [
        {
          value: 'hcjd',
          label: '皇城街道办事处'
        },
        {
          value: 'dnjd',
          label: '大南街道办事处'
        },
        {
          value: 'dxjd',
          label: '大西街道办事处'
        },
        {
          value: 'dnjjd',
          label: '大南街道办事处'
        }
      ]
    },
    {
      value: 'jiangsu',
      label: '江苏',
      children: [
        {
          value: 'nanjing',
          label: '南京',
          children: [
            {
              value: 'fuzimiao',
              label: '夫子庙'
            }
          ]
        },
        {
          value: 'suzhou',
          label: '苏州',
          children: [
            {
              value: 'zhuozhengyuan',
              label: '拙政园'
            },
            {
              value: 'shizilin',
              label: '狮子林'
            }
          ]
        }
      ]
    }]
}
