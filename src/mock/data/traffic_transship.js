export default {
  errcode: 200,
  errmsg: 'ok',
  data: {
    taskNo: 'task_001', // 任务编码
    taskType: '1', // 管控方式
    taskStatus: '1', // 任务状态
    startAddress: '大连市沙河口区', // 始发地址
    startAddressTel: '84335147', // 始发地联系电话
    startAddressContact: '包子', // 始发地联系人
    destinationContact: '馒头', // 目的地联系人
    destinationTel: '8433376778', // 目的地电话
    destinationAddress: '大连市沙河口区', // 目的地地址
    controlStartData: '2022-05-04', // 管控开始日期
    controlEndData: '2022-05-04', // 管控解除日期
    transportDriver: '西红柿', // 转运司机
    transportCar: 'TOYOTA' // 转运车辆
  }
}
