export default {
    errcode: 200,
    errmsg: 'ok',
    data: {
        list: [{
                id: '1',
                taskNo: '01', //任务编码
                taskType: '1', //管控方式
                taskStatus: '1', //任务状态
                startAddress: '大连沙河口', //始发地址
                startAddressTel: '13722341234', //始发地联系电话
                startAddressContact: '小王', //始发地联系人
                destinationContact: '豆包', //目的地联系人
                destinationTel: '1234562345', //目的地电话
                destinationAddress: '大连中山区', //目的地地址
                transportDriver: '王默默', //转运司机
            },
            {
                id: '2',
                taskNo: '02', //任务编码
                taskType: '0', //管控方式
                taskStatus: '1', //任务状态
                startAddress: '大连沙河口', //始发地址
                startAddressTel: '13722341234', //始发地联系电话
                startAddressContact: '小王', //始发地联系人
                destinationContact: '豆包', //目的地联系人
                destinationTel: '1234562345', //目的地电话
                destinationAddress: '大连中山区', //目的地地址
                transportDriver: '王默默', //转运司机
            },
            {
                id: '3',
                taskNo: '03', //任务编码
                taskType: '2', //管控方式
                taskStatus: '1', //任务状态
                startAddress: '大连沙河口', //始发地址
                startAddressTel: '13722341234', //始发地联系电话
                startAddressContact: '小王', //始发地联系人
                destinationContact: '豆包', //目的地联系人
                destinationTel: '1234562345', //目的地电话
                destinationAddress: '大连中山区', //目的地地址
                transportDriver: '王默默', //转运司机
            },
            {
                id: '4',
                taskNo: '04', //任务编码
                taskType: '0', //管控方式
                taskStatus: '3', //任务状态
                startAddress: '大连沙河口', //始发地址
                startAddressTel: '13722341234', //始发地联系电话
                startAddressContact: '小王', //始发地联系人
                destinationContact: '豆包', //目的地联系人
                destinationTel: '1234562345', //目的地电话
                destinationAddress: '大连中山区', //目的地地址
                transportDriver: '王默默', //转运司机
            },
            {
                id: '5',
                taskNo: '05', //任务编码
                taskType: '2', //管控方式
                taskStatus: '0', //任务状态
                startAddress: '大连沙河口', //始发地址
                startAddressTel: '13722341234', //始发地联系电话
                startAddressContact: '小王', //始发地联系人
                destinationContact: '豆包', //目的地联系人
                destinationTel: '1234562345', //目的地电话
                destinationAddress: '大连中山区', //目的地地址
                transportDriver: '王默默', //转运司机
            },
        ]

    }
}