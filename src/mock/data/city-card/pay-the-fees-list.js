export default {
  errcode: 200,
  errmsg: 'ok',
  data: [
    { id: 1, name: '水费', icon: 'gold-coin-o', path: '/city-card-pay-the-fees-electric-charge' },
    { id: 2, name: '电费', icon: 'location-o', path: '/city-card-pay-the-fees-electric-charge' },
    { id: 3, name: '煤气费', icon: 'like-o', path: '/city-card-pay-the-fees-electric-charge' },
    { id: 4, name: '供暖费', icon: 'star-o', path: '/city-card-pay-the-fees-electric-charge' },
    { id: 5, name: '电话费', icon: 'phone-o', path: '/city-card-pay-the-fees-electric-charge' }
  ]
}
