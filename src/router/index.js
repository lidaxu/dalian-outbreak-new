import Vue from 'vue'
import Router from 'vue-router'
import routes from './routers'

Vue.use(Router)
const router = new Router({
  scrollBehavior (to, from, savedPosition) {

  },
  routes,
  mode: 'hash'
})

router.beforeEach((to, from, next) => {
  // if (to.name === 'login' && key) {
  //   next({
  //     name: 'home'
  //   })
  // }
  if (to.meta.title) {
    document.title = to.meta.title
  }
  // let bodySrcollTop = document.documentElement.scrollTop
  // console.log(bodySrcollTop, 'bodySrcollTopbodySrcollTop')
  // chrome
  document.body.scrollTop = 0
  // firefox
  document.documentElement.scrollTop = 0
  // safari
  window.pageYOffset = 0
  next()
})

// router.afterEach(to => {
//   window.scrollTo(0, 0)
// })

export default router
