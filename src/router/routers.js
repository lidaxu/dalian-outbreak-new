const base = [
  {
    path: '/',
    name: 'main',
    component: () => import('@/views/login.vue'),
    meta: {
      title: '人员信息'
    }
  },
  {
    path: '/report',
    name: 'report',
    children: [],
    meta: {
      title: '活动轨迹报备'
    },
    component: () => import('@/views/reporting/report')
  },
  {
    path: '/result',
    name: 'result',
    children: [],
    meta: {
      title: '活动轨迹报备成功'
    },
    component: () => import('@/views/reporting/result')
  },
  {
    path: '/configuration',
    name: 'configuration',
    children: [],
    meta: {
      title: '登记选择'
    },
    component: () => import('@/views/home/configuration')
  },
  {
    path: '/jwhome',
    name: 'jwhome',
    children: [],
    meta: {
      title: '境外机场'
    },
    component: () => import('@/views/home/jwhome')
  },
  {
    path: '/ywhome',
    name: 'ywhome',
    children: [],
    meta: {
      title: '域外机场'
    },
    component: () => import('@/views/home/ywhome')
  },
  {
    path: '/hczhome',
    name: 'hczhome',
    children: [],
    meta: {
      title: '火车站'
    },
    component: () => import('@/views/home/hczhome')
  },
  {
    path: '/gkhome',
    name: 'gkhome',
    children: [],
    meta: {
      title: '港口'
    },
    component: () => import('@/views/home/gkhome')
  },
  {
    path: '/kyzhome',
    name: 'kyzhome',
    children: [],
    meta: {
      title: '客运站'
    },
    component: () => import('@/views/home/kyzhome')
  },
  {
    path: '/jtkkhome',
    name: 'jtkkhome',
    children: [],
    meta: {
      title: '交通卡口'
    },
    component: () => import('@/views/home/jtkkhome')
  },
  {
    path: '/jwdetail',
    name: 'jwdetail',
    children: [],
    meta: {
      title: '境外机场'
    },
    component: () => import('@/views/home/jwdetail')
  },
  {
    path: '/ywdetail',
    name: 'ywdetail',
    children: [],
    meta: {
      title: '域外机场'
    },
    component: () => import('@/views/home/ywdetail')
  },
  {
    path: '/hczdetail',
    name: 'hczdetail',
    children: [],
    meta: {
      title: '火车站'
    },
    component: () => import('@/views/home/hczdetail')
  },
  {
    path: '/gkdetail',
    name: 'gkdetail',
    children: [],
    meta: {
      title: '港口'
    },
    component: () => import('@/views/home/gkdetail')
  },
  {
    path: '/kyzdetail',
    name: 'kyzdetail',
    children: [],
    meta: {
      title: '客运站'
    },
    component: () => import('@/views/home/kyzdetail')
  },
  {
    path: '/jtkkdetail',
    name: 'jtkkdetail',
    children: [],
    meta: {
      title: '交通卡口'
    },
    component: () => import('@/views/home/jtkkdetail')
  },
  {
    path: '/selfhelp',
    name: 'selfhelp',
    children: [],
    meta: {
      title: '社区自主报备'
    },
    component: () => import('@/views/home/selfhelp')
  },
  {
    path: '/qrcode',
    name: 'qrcode',
    children: [],
    meta: {
      title: '社区自主报备'
    },
    component: () => import('@/views/home/selfhelpQrCode')
  },
  {
    path: '/record-new',
    name: 'record-new',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/record-new-hc')
  },

  {
    path: '/record',
    name: '/record',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/recordNewLogin')
  },

  {
    path: '/recordNewLogin',
    name: 'recordNewLogin',
    children: [],
    meta: {
      title: '入连报备'
    },
    component: () => import('@/views/home/recordNewLogin')
  },
  {
    path: '/recordNewMy',
    name: 'recordNewMy',
    children: [],
    meta: {
      title: '我的信息'
    },
    component: () => import('@/views/home/recordNewMy')
  },
  {
    path: '/recordNewMy-hc',
    name: 'recordNewMy-hc',
    children: [],
    meta: {
      title: '我的信息'
    },
    component: () => import('@/views/home/recordNewMy-hc')
  },
  {
    path: '/recordNewOk',
    name: 'recordNewOk',
    children: [],
    meta: {
      title: '报备完成'
    },
    component: () => import('@/views/home/recordNewOk-hc')
  },
  {
    path: '/recordNewOk-hc',
    name: 'recordNewOk-hc',
    children: [],
    meta: {
      title: '报备完成'
    },
    component: () => import('@/views/home/recordNewOk-hc')
  },

  {
    path: '/recordNewInfo-hc',
    name: 'recordNewInfo-hc',
    children: [],
    meta: {
      title: '来连自主报备'
    },
    component: () => import('@/views/home/recordNewInfo-hc')
  },
  {
    path: '/recordNewInfo',
    name: 'recordNewInfo',
    children: [],
    meta: {
      title: '来连自主报备'
    },
    component: () => import('@/views/home/recordNewInfo')
  },
  {
    path: '/recordNewQrCode',
    name: 'recordNewQrCode',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/recordNewQrCode')
  },
  {
    path: '/recordNewQrCode-hc',
    name: 'recordNewQrCode-hc',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/recordNewQrCode-hc')
  },
  {
    path: '/record',
    name: 'recordNewLogin_one',
    children: [],
    meta: {
      title: '入连报备'
    },
    component: () => import('@/views/home/recordNewLogin')
  },
  {
    path: '/record',
    name: 'record',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/record')
  },
  {
    path: '/record-info',
    name: 'record-info',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/record-info')
  },
  {
    path: '/record-qrinfo',
    name: 'record-qrinfo',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/record-qrinfo')
  },
  {
    path: '/record-time',
    name: 'record-time',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/record-time')
  },
  {
    path: '/recordQrCode',
    name: 'recordQrCode',
    children: [],
    meta: {
      title: '来连人员自主报备'
    },
    component: () => import('@/views/home/recordQrCode')
  },
  {
    path: '/student-register',
    name: 'student-register',
    children: [],
    meta: {
      title: '学生注册'
    },
    component: () => import('@/views/home/student-register')
  },
  {
    path: '/student-menu',
    name: 'student-menu',
    children: [],
    meta: {
      title: '首页'
    },
    component: () => import('@/views/home/student-menu')
  },
  {
    path: '/student-information',
    name: 'student-information',
    children: [],
    meta: {
      title: '我的信息'
    },
    component: () => import('@/views/home/student-information')
  },
  {
    path: '/student-resident',
    name: 'student-resident',
    children: [],
    meta: {
      title: '共同居住人'
    },
    component: () => import('@/views/home/student-resident')
  },
  {
    path: '/student-health',
    name: 'student-health',
    children: [],
    meta: {
      title: '每日健康填报'
    },
    component: () => import('@/views/home/student-health')
  },
  {
    path: '/traffic_transship',
    name: 'traffic_transship',
    children: [],
    meta: {
      title: '交通转运'
    },
    component: () => import('@/views/home/traffic-transship')
  },
  {
    path: '/traffic_transship_info',
    name: 'traffic_transship_info',
    children: [],
    meta: {
      title: '交通转运详情'
    },
    component: () => import('@/views/home/traffic_transship_info')
  },
  {
    path: '/traffic_transship_login',
    children: [],
    meta: {
      title: '交通转运登录'
    },
    component: () => import('@/views/home/traffic_transship_login')
  },
  // {
  //   path: '/traffic_transship_list',
  //   name: 'traffic_transship_list',
  //   children: [],
  //   meta: {
  //     title: '交通转运列表'
  //   },
  //   component: () => import('@/views/home/traffic_transship_list')
  // },
  {
    path: '/traffic_transship_list',
    name: 'traffic_transship_list',
    children: [],
    meta: {
      title: '交通转运清单'
    },
    component: () => import('@/views/home/traffic_transship_info')
  },
  {
    path: '/return-school',
    name: 'return-school',
    children: [],
    meta: {
      title: '返校申请'
    },
    component: () => import('@/views/home/return-school')
  },
  {
    path: '/truck-filing',
    name: 'truck-filing',
    children: [],
    meta: {
      title: '登/离岛报备'
    },
    component: () => import('@/views/home/truck-filing')
  },

  {
    path: '/truck-filing-search',
    name: 'truck-filing-search',
    children: [],
    meta: {
      title: '登/离岛报备查询'
    },
    component: () => import('@/views/home/truck-filing-search')
  },
  {
    path: '/truck-filing-info',
    name: 'truck-filing-info',
    children: [],
    meta: {
      title: '登/离岛报备详情'
    },
    component: () => import('@/views/home/truck-filing-info')
  },
  {
    path: '/truck-filing-info-new',
    name: 'truck-filing-info-new',
    children: [],
    meta: {
      title: '登/离岛报备详情'
    },
    component: () => import('@/views/home/truck-filing-info-new')
  },
  {
    path: '/home3',
    name: 'home3',
    children: [],
    meta: {
      title: '社区'
    },
    component: () => import('@/views/home/home3')
  },
  {
    path: '/home2',
    name: 'home2',
    children: [],
    meta: {
      title: '火车站'
    },
    component: () => import('@/views/home/home2')
  },
  // 补采采样点信息表
  {
    path: '/excel',
    name: 'excel',
    children: [],
    meta: {
      title: '补采采样点信息表'
    },
    component: () => import('../views/excel/excel.vue')
  }
]

export default base
