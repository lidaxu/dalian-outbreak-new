import axios from '@/libs/api.request'

export const mLogin = (url) => {
  if (url) {
    url += '/wl/mLogin'
  } else {
    url = '/wl/mLogin'
  }
  return axios.request({
    url: url,
    type: 'get',
    data: {
      // openid: 'ohOcC0iR8_VB7QOi8FTMQNhbFgA4',
      // unionid: 'opIXV5x8YNwEv_9WT4eEkan6qmFM',
      openid: '',
      unionid: ''
    }
  })
}
