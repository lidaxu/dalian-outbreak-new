import axios from '@/libs/api.request'
import config from '@/config'
import { getDomain } from '@/libs/tools'
import { Notify } from 'vant'


export const getMobileRegisterInfoHc = (data) => {
  return axios.request({
    url: '/api/ac/register/registerService/findRegisterInfo',
    data: data,
    type: 'post'
  })
}


export const getMobileRegisterInfo = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/getMobileRegisterInfo',
    data: data,
    type: 'post'
  })
}

export const getQrUrl = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/getQrUrl',
    data: data,
    type: 'post'
  })
}

//刷新code
export const editReloadQueryCode = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/editReloadQueryCode',
    data: data,
    type: 'post'
  })
}

//行政区划
export const editQueryCode = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/editQueryCode',
    data: data,
    type: 'post'
  })
}



//行政区划
export const getDistrictArea = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/sysService/getDistrictArea',
    data: data,
    type: 'post'
  })
}

//社区自主报备
export const helpUserNewInsert = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/insertRegisterInfo',
    data: data,
    type: 'post'
  })
}



//社区自主报备
export const helpUserNewInsertHc = (data) => {
  return axios.request({
    url: '/api/ac/register/registerService/insertRegisterInfo',
    data: data,
    type: 'post'
  })
}

//社区自主报备
export const helpUserInsert = (data) => {
  return axios.request({
    // url: 'api/ac/registerdalian/helpUserService/helpUserInsert',
    url: '/api/ac/registerdalian/selfHelpRecordUserService/insertRecordUser',
    data: data,
    type: 'post'
  })
}
//生成二维码
export const helpUserQrCode = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/passengerStation/qrUserCode',
    data: data,
    type: 'post'
  })
}

//生成二维码
export const helpUserNewQrCode = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/trafficService/getQrCode',
    data: data,
    type: 'post'
  })
}

export const getCodeNameByCode = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/selfHelpRecordUserService/getCommunityNameByCode',
    data: data,
    type: 'post'
  })
}
