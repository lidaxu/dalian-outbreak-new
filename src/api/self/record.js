import axios from '@/libs/api.request'
import config from '@/config'
import { getDomain } from '@/libs/tools'
import { Notify } from 'vant'



export const getSign = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/getHadoopService/getSign',
    data: data,
    type: 'post'
  })
}

export const upLoadFiles = (data) => {
  return axios.request({
    // url: 'http://daliandl.lngov.top/api/ac/file/fileCommonService/upLoadFiles',

    url: 'http://59.46.238.106:38012/api/ac/file/fileCommonService/upLoadFiles',
    // url: 'http://dalianoutbreaknew.lngov.top/api/ac/file/fileCommonService/upLoadFiles',
    // url: 'https://cegaiup.qstb.top/api/ac/file/fileCommonService/upLoadFiles',
    // url: 'api/ac/file/fileCommonService/upLoadFiles',
    data: data,
    type: 'post'
  })
}

export const helpUserInsert = (data) => {
  return axios.request({
    url: 'api/ac/registerdalian/selfHelpRecordUserService/insertRecordUser',
    data: data,
    type: 'post'
  })
}
//生成二维码
export const recordUserQrCode = (data) => {
  return axios.request({
    url: 'api/ac/registerdalian/passengerStation/qrUserCodeRecord',
    data: data,
    type: 'post'
  })
}
export const destinationByCity = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/selfHelpRecordUserService/getCommunityList',
    data: data,
    type: 'post'
  })
}


//根据三要素查询
export const getNameCodeMobeil = (data) => {
  return axios.request({
    url: 'api/ac/registerdalian/selfHelpRecordUserService/selfHelpRecordTime',
    data: data,
    type: 'post'
  })
}

//入连填报扫描二维码id回显
export const getRecordInfoByIdTime = (id, time) => {
  return axios.request({
    url: '/api/ac/registerdalian/selfHelpRecordUserService/getQrRecordInfoById?id=' + id + '&time=' + time,
    type: 'get'
  })
}

//入连填报扫描二维码id回显
export const getRecordInfoById = (id) => {
  return axios.request({
    url: '/api/ac/registerdalian/selfHelpRecordUserService/getRecordInfoById?id=' + id,
    type: 'get'
  })
}



//入连填报扫描二维码id回显
export const getRecordInfoNewById = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/tRegisterBaseInfoService/getRegisterBaseInfo',
    data: data,
    type: 'post'
  })
}




//区街道社区
export const getRegionalDivisionJson = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/regionalDivisionService/getRegionalDivisionJson',
    // url: '/api/ac/registerdalian/registerdalianSysService/registerOfficeList',
    data: data,
    type: 'post'
  })
}



//区街道社区
export const registerOfficeListNEW = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/registerdalianSysService/registerOfficeListNEW',
    data: data,
    type: 'post'
  })
}


//区街道社区
export const registerOfficeList = (data) => {
  return axios.request({
    //  registerOfficeList  
    url: '/api/ac/registerdalian/registerdalianSysService/registerOfficeList',
    data: data,
    type: 'post'
  })
}



