import axios from '@/libs/api.request'
import config from '@/config/index'

export const areaReport = data => {
  return axios.request({
    url: config.baseUrlLD + '/api/ac/symjld/ldxrAreaService/findList',
    data: data,
    type: 'post'
  })
}

export const insertReport = data => {
  return axios.request({
    url: config.baseUrlLD + '/api/ac/symjld/ldxrFilingService/insertData',
    data: data,
    type: 'post'
  })
}
export const updateData = data => {
  return axios.request({
    url:
      config.baseUrlLD + '/api/ac/symjld/ldxrFilingService/updateData',
    data: data,
    type: 'post'
  })
}
