import axios from '@/libs/api.request'
import config from '@/config'
import { getDomain } from '@/libs/tools'
import { Notify } from 'vant'
/* 授权 */
export const mnlogin = () => {
  return axios.request({
    url: '/v1/un/mnlogin',
    type: 'get'
  })
}
/* 政务通事项列表 */
export const todolist = (data) => {
  return axios.request({
    url: '/api/uc/csc/auditItemService/todolist',
    data: data,
    type: 'post'
  })
}
// 新事项详情
export const itemInfo = (data) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/itemInfo',
    data: data,
    type: 'post'
  })
}
// 政务通事项详情
export const serviceInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditItemService/itemInfo',
    data: data,
    type: 'post'
  })
}
// 要件详情
export const documentInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/audititem/documentlist',
    data: data,
    type: 'post'
  })
}
export const orderInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditorder/getorderbyuid',
    data: data,
    type: 'post'
  })
}
// 下单草稿
export const createOrder = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/createOrderByUser',
    data: data,
    type: 'post'
  })
}
// 提交审核
export const updateOrder = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/updateOrder',
    data: data,
    type: 'post'
  })
}
// 订单列表
export const wxorderlist = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditorder/wxorderlist',
    data: data,
    type: 'get'
  })
}
// 图片上传
export const objectToOss = (data) => {
  return axios.request({
    url: 'Api/Api/upBase64ToOss',
    data: data,
    php: true,
    // headers: { 'Content-Type': 'multipart/form-data;' },
    type: 'post'
  })
}

// 新上传图片
export const uploaderImg = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/uploadPic',
    data: data,
    type: 'post'
  })
}
// 更改要件
export const updateDoc = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/userRegisterService/updatePhoto',
    data: data,
    type: 'post'
  })
}
// 修改草稿
export const updateOrderInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/cdc/auditorder/updateOrderByUser',
    data: data,
    type: 'post'
  })
}
// 要件提报
export const setOrderTB = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/setOrderTB',
    data: data,
    type: 'post'
  })
}
// -------------- 对接易迅 -------------------------//
// 证件类型
export const docType = (data) => {
  return axios.request({
    url: '/api/uc/csc/createPostService/getAppraising',
    data: data,
    type: 'post'
  })
}
export const getorderbyuid = (data) => {
  return axios.request({
    url: '/api/us/csc/auditorder/getorderbyuid',
    data: data,
    type: 'post'
  })
}
// 区别
export const qu = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/createPostService/getAdministrativePlanning',
    data: data,
    type: 'post'
  })
}
// 回显
export const huixian = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/createPostService/getBack',
    data: data,
    type: 'post'
  })
}
// -------------- 对接易迅 -------------------------//
export const sendMessage = (data) => {
  return axios.request({
    url: '/api/uc/csc/sendMessageService/sendMessage',
    data: data,
    type: 'post'
  })
}
export const checkMessage = (data) => {
  return axios.request({
    url: '/api/uc/csc/sendMessageService/checkMessage',
    data: data,
    type: 'post'
  })
}
// 用户信息
export const userInfo = () => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/user/getuserinfo1',
    type: 'get'
  })
}
// 获取token
export const getJuYouToken = () => {
  return axios.request({
    url: 'api/uc/ctc/secretTokenService/getToken',
    type: 'get'
  })
}
// 免密登录
export const loginForCTC = (data, fun) => {
  axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/loginService/loginForCTC',
    type: 'post',
    data: data,
    headerConfig: { withCredentials: true }
  }).then(ret => {
    if (ret.data.errcode === 0) {
      let url = getDomain(config.baseUrlMZ + '/api/uc/csc/loginService/loginForCTC')
      fun(ret, url)
    } else {
      Notify({
        message: ret.data.errmsg
      })
    }
  })
}
// 提交页面
export const createAgentAppoint = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditagentappoint/createAgentAppoint',
    type: 'post',
    data: data
  })
}
// 社区下拉列表
export const findAllCommuntityList = () => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/audititem/findAllCommuntityList',
    type: 'get'
  })
}
// 预约详情
export const getAgentAppointInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditagentappoint/getAgentAppointInfo',
    type: 'post',
    data: data
  })
}
// 预约列表
export const findAppointList = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditagentappoint/findAppointList',
    type: 'post',
    data: data
  })
}
// 用户投诉
export const addFeedback = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/addFeedback',
    type: 'post',
    data: data
  })
}
// 投诉详情
export const getFeedbackInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditorder/getFeedbackInfo',
    type: 'post',
    data: data
  })
}
// 评分
export const setStarByOrderCode = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditorder/setStarByOrderCode',
    type: 'get',
    data: data
  })
}
// 获取用户信息
export const userLoginInfo = () => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/user/getuserinfo1',
    type: 'get'
  })
}
// 下单(保存)
export const saveOrderByUser = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/saveOrderByUser',
    type: 'post',
    data: data
  })
}
// 提报
export const saveOrderAndTB = (data) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/submitOrder',
    type: 'post',
    data: data
  })
}
// 社区列表
export const getMailLevelList = (data) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/getMailLevelList',
    data: data,
    type: 'get'
  })
}
// 订单详情
export const getOrderInfoByPhone = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditOrderService/getOrderInfoByPhone',
    data: data,
    type: 'post'
  })
}
// 社区列表
export const getSheQuList = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/us/csc/auditmail/getSheQuList',
    data: data,
    type: 'get'
  })
}
// 残疾人查询
export const findCanlian = (data) => {
  return axios.request({
    url: 'https://iview.tripln.top/api/uc/csc/streeAllServiceImpl/findCanlian',
    data: data,
    type: 'get'
  })
}
// 低保查询
export const getAssistanceInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/createPostService/getAssistanceInfo',
    data: data,
    type: 'get'
  })
}
// 户籍查询
export const getRegisterInfo = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/auditRegisterService/getRegisterInfo',
    data: data,
    type: 'post'
  })
}
// 获取个人电子证照列表
export const createPersonListForLogin = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/dzzzPersonService/createPersonListForLogin',
    data: data,
    type: 'get'
  })
}
// 获取个人电子证照接口
export const createGetFile = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/uc/csc/dzzzPersonService/createGetFile',
    data: data,
    type: 'post'
  })
}
// 申请重度残疾人护理补贴
export const UpdateOrder = (data) => {
  return axios.request({
    url: config.baseUrlMZ + '/api/ac/csc/auditOrderService/updateOrder',
    data: data,
    type: 'post'
  })
}
// 查询低保证号
export const createPostNew = (param) => {
  return axios.request({
    url: '/api/ac/csc/createPostService/createPostNew',
    method: 'post',
    data: param
  })
}
// 全市通办手机下单 新
export const orderByUser = (param) => {
  return axios.request({
    // url: '/api/uc/ctc/publicMZService/orderByUser',
    url: '/api/uc/ctcyq/publicAdministrationService/orderByUser',
    method: 'post',
    data: param
  })
}
// 全市通办个人订单列表 新
export const newwxorderlist = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/wxorderlist',
    method: 'post',
    data: param
  })
}
// 全市通办个人订单详情 新
export const newgetOrderInfoByPhone = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/getOrderInfoByPhone',
    method: 'post',
    data: param
  })
}
// 全市通办获取签名 新
export const getSign = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/getSign',
    method: 'post',
    data: param
  })
}
// 全市通办通过company_code获取上级信息
export const getMailInfo = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/getMailInfo',
    method: 'post',
    data: param
  })
}
// 全市通办上传图片
export const uploadPicture = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/uploadPicture',
    method: 'post',
    data: param
  })
}
// 专业
export const getXLMenuList = (param) => {
  return axios.request({
    url: '/api/uc/ctc/publicMZService/getXLMenuList',
    method: 'post',
    data: param
  })
}
// 获取时间段
export const findLinenumList = (param) => {
  return axios.request({
    url: 'http://iview.tripln.top/api/uc/csc/lineUpService/findLinenumList',
    method: 'post',
    data: param
  })
}
// 预约
export const createLineUpWeb = (param) => {
  return axios.request({
    url: 'http://iview.tripln.top/api/uc/csc/lineUpService/createLineUpWeb',
    method: 'post',
    data: param
  })
}
// 2021年8月2日 新接口
export const establishUserMattersOrder = (param) => { // 下单接口
  return axios.request({
    url: '/api/uc/sst/sstHandlingMattersService/establishUserMattersOrder',
    method: 'post',
    data: param
  })
}
export const findOfficeList = (param) => { // 查询机构接口
  return axios.request({
    url: '/api/uc/sst/sstHandlingMattersService/findOfficeList',
    method: 'post',
    data: param
  })
}
export const itemInfoQstb = (param) => { // 事项详情接口
  return axios.request({
    url: '/api/uc/sst/sstHandlingMattersService/itemInfo',
    method: 'post',
    data: param
  })
}
export const getSignSst = (param) => { // 获取上传图片获取签名接口
  return axios.request({
    url: '/api/uc/sst/sstHandlingMattersService/getSign',
    method: 'post',
    data: param
  })
}
/* 公众号serveId上传图片 */
export const uploadfile_ = (data) => {
  return axios.request({
    url: '/api/uc/mobile/uploadFileService/uploadfile',
    type: 'post',
    data: data
  })
}
/* 上传图片App */
export const putBase64ImgObject = (data) => {
  return axios.request({
    url: '/api/uc/mobile/uploadFileService/putBase64ImgObject',
    type: 'post',
    data: data
  })
}
// 时间转字符串 年月日时分秒
export function datetime2str (date, containtime) {
  let time1 = date.getFullYear() + ''
  let month = date.getMonth() + 1
  let day = date.getDate()
  let hh = date.getHours()
  let mm = date.getMinutes()
  let ss = date.getSeconds()
  time1 += month < 10 ? '0' + month : month
  time1 += day < 10 ? '0' + day : day
  time1 += hh < 10 ? '0' + hh : hh
  time1 += mm < 10 ? '0' + mm : mm
  time1 += ss < 10 ? '0' + ss : ss
  return time1
}
export const createUpLoadFiles = (url, data) => {
  return axios.request({
    url: url,
    data: data,
    method: 'post'
  })
}
export const getOpenidMsg = (data) => {
  return axios.request({
    url: '/api/uc/ctcyq/ictcService/getOpenidMsg',
    data: data,
    method: 'post'
  })
}
export const getuserIdKey = (data) => {
  return axios.request({
    url: '/api/uc/ctcyq/wxUserIdKeyService/getuserIdKey',
    data: data,
    method: 'post'
  })
}
// export const upCerStatus = (data) => {
//   return axios.request({
//     url: '/api/uc/ctcyq/ksOcrLivingValidationService/upCerStatus',
//     data: data,
//     method: 'post'
//   })
// }
export const checkUserIdKey = (data) => {
  return axios.request({
    url: '/api/uc/ctcyq/ksOcrLivingValidationService/upCerStatus',
    data: data,
    type: 'post'
  })
}
export const updateOrderParams = (data) => { // 更新订单
  return axios.request({
    url: '/api/uc/sst/sstHandlingMattersService/updateOrderParams',
    data: data,
    type: 'post'
  })
}
export const getCompanyInfo = (data) => { // 根据company_code获取区域信息
  return axios.request({
    url: '/api/uc/sst/sstUserCommitOrderService/getCompanyInfo',
    data: data,
    type: 'post'
  })
}
export const appOcr = (data) => {
  return axios.request({
    url: '/api/uc/ctcyq/ksOcrLivingValidationService/appOcr',
    data: data,
    type: 'post'
  })
}
// app比对人脸,原来app用的，在微信借用
export const faceCheck = (data) => {
  return axios.request({
    url: 'api/uc/ctcyq/appLoginService/faceCheck',
    data: data,
    loading: true,
    type: 'post'
  })
}

// 用户状态=2 比对人脸
export const faceCheck23 = (data) => {
  return axios.request({
    url: 'api/uc/ctcyq/appLoginService/faceCheck23',
    data: data,
    loading: true,
    type: 'post'
  })
}
// app比对人脸
export const selectFaceComparison = (data) => {
  return axios.request({
    url: '/api/uc/ctcyq/medicalInsuranceService/selectFaceComparison',
    data: data,
    type: 'post'
  })
}
