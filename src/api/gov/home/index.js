import axios from '@/libs/api.request'
export const addPersonnelSq = (data) => { //社区人员信息录入
    return axios.request({
        url: '/api/ac/registerdalian/communityInfoCollectionService/insertData',
        type: 'post',
        data: data
    })
}

export const addRailwayStation = (data) => { // 最新火车站信息录入
    return axios.request({
        url: '/api/ac/registerdalian/railwaystationInfoCollectionService/insertData',
        type: 'post',
        data: data
    })
}
export const addPersonnel = (data) => { // 人员信息录入
    return axios.request({
        url: '/api/ac/dles/outboundService/addPersonnel',
        type: 'post',
        data: data
    })
}
export const addAreaEnterdlAirport = (data) => { // 境外信息录入
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addAreaEnterdlAirport',
        type: 'post',
        data: data
    })
}
export const getAreaEnterdlPlatformList = (data) => { // 境外
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/getAreaEnterdlPlatformList',
        type: 'post',
        data: data
    })
}
export const addAbroadDaLian = (data) => { // 域外入连
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addAbroadDaLian',
        type: 'post',
        data: data
    })
}
export const getAbroadEnterdlPlatformList = (data) => { // 域外
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/getAbroadEnterdlPlatformList',
        type: 'post',
        data: data
    })
}
export const addEnterTrainPlatform = (data) => { // 火车入境
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addEnterTrainPlatform',
        type: 'post',
        data: data
    })
}
export const getEnterTrainPlatformList = (data) => { // 火车入境
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/getEnterTrainPlatformList',
        type: 'post',
        data: data
    })
}
export const addPortInfo = (data) => { // 港口
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addPortInfo',
        type: 'post',
        data: data
    })
}
export const queryHaven = (data) => { // 港口
    return axios.request({
        url: '/api/ac/registerdalian/havenService/queryHaven',
        type: 'post',
        data: data
    })
}
export const addPassengerStation = (data) => { // 客运站
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addPassengerStation',
        type: 'post',
        data: data
    })
}
export const queryStation = (data) => { // 客运站
    return axios.request({
        url: '/api/ac/registerdalian/passengerStation/queryStation',
        type: 'post',
        data: data
    })
}
export const addTrafficCheckPoint = (data) => { // 交通卡口
    return axios.request({
        url: '/api/ac/registerdalian/enterPlatform/addTrafficCheckPoint',
        type: 'post',
        data: data
    })
}
export const queryTraffic = (data) => { // 交通卡口
    return axios.request({
        url: '/api/ac/registerdalian/trafficService/queryTraffic',
        type: 'post',
        data: data
    })
}
export const findMainareaInfoList = (data) => { // 重点关注管控地区列表（不分页）
    return axios.request({
        url: '/api/as/dles/mainarea/findMainareaInfoList',
        type: 'post',
        data: data
    })
}
export const userLoginMobile = (data) => { // 登录
    return axios.request({
        url: '/api/uc/sc/loginService/userLogin_encrypt',
        // url: '/api/uc/sc/loginService/userLogin2',
        type: 'post',
        data: data
    })
}
export const genVerifyCode = (data) => { // 获取验证码
    return axios.request({
        url: '/api/uc/sc/loginService/genVerifyCode',
        type: 'post',
        data: data
    })
}
export const getFilingInfo = (data) => { // 查询备案信息
    return axios.request({
        url: '/api/ac/dles/outboundService/getFilingInfo',
        type: 'post',
        data: data
    })
}

export const trafficTransshipDetail = (data) => { // 交通转运
    return axios.request({
        url: '/mock/get/some/trafficTransshipDetail/',
        type: 'get',
        data: data
    })
}

export const trafficTransshipList = (data) => { // 交通转运列表
    return axios.request({
        url: '/mock/get/some/trafficTransshipList/',
        type: 'get',
        data: data
    })
}