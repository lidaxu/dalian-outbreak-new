import axios from '@/libs/api.request'
import config from '@/config'
import { getDomain } from '@/libs/tools'
import { Notify } from 'vant'

// 货车来连备案
export const addTraffic = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/trafficService/addTraffic',
    data: data,
    type: 'post'
  })
}
// 查看当前用户48小时内核酸证明
export const check48 = (data) => {
  return axios.request({
    url: 'api/ac/registerdalian/nucleinService/queryNuclein',
    data: data,
    type: 'post'
  })
}

// 扫码根据id回显详情信息
export const getTruckFilingById = (data) => {
  return axios.request({
    url: '/api/ac/registerdalian/trafficService/queryTrafficInfoById?id=' + data,
    type: 'get'
  })
}

// 扫码根据id回显详情信息
export const getByTruckCardId = (data) => {
  return axios.request({
    url: '/api/ac/register/truckInfoService/selectId',
    data: data,
    type: 'post'
  })
}
// 根据身份证查看长海县报备信息
export const queryTrafficInfoByCardNo = (params) => {
  return axios.request({
    url: '/api/ac/registerdalian/trafficService/queryTrafficInfoByCardNo',
    params: params,
    type: 'GET'
  })
}
// 扫码根据id回显详情信息
export const getByTruckId = (data) => {
  return axios.request({
    url: 'api/ac/register/truckInfoService/selectPlate',
    data: data,
    type: 'post'
  })
}
