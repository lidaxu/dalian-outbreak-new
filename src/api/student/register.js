import axios from '@/libs/api.request'
// 学生登录
export const register = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/userRegister',
    data: data,
    type: 'post'
  })
}
// 学校下拉
export const getSchoolList = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/getSchoolList',
    data: data,
    type: 'post'
  })
}
// 获取班级下拉选
export const getClassList = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/getClassList',
    data: data,
    type: 'post'
  })
}

// 添加共同居住人
export const addCohabitantInfo = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/addCohabitantInfo',
    data: data,
    type: 'post'
  })
}

// 根据当前学生id 查看共同居住人列表
export const getAllCohabitantByUserId = (userInfoId) => {
  return axios.request({
    url: 'api/ac/school/schoolService/getAllCohabitantByUserId?userInfoId=' + userInfoId,
    type: 'get'
  })
}
// 根据id 删除共同居住人
export const deleteCohabitant = (userId) => {
  return axios.request({
    url: 'api/ac/school/schoolService/deleteCohabitant?id=' + userId,
    type: 'get'
  })
}
// 学生每日填报
export const everDayFillIn = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/everDayFillIn',
    data: data,
    type: 'post'
  })
}

// 获取家庭id
export const generateFamilyId = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/generateFamilyId',
    data: data,
    type: 'post'
  })
}

// 根据id查看我的信息
export const getUserInfo = (sysUserId) => {
  return axios.request({
    url: 'api/ac/school/schoolService/getUserInfo?sysUserId=' + sysUserId,
    type: 'get'
  })
}

// 修改个人基本信息
export const editSysUserInfo = (data) => {
  return axios.request({
    url: 'api/ac/school/schoolService/editSysUserInfo',
    data: data,
    type: 'post'
  })
}

// 每日健康填报回显json
export const getAllForFillIn = (familyId) => {
  return axios.request({
    url: '/api/ac/school/schoolService/getAllForFillIn?familyId=' + familyId,
    type: 'get'
  })
}

// 根据家庭id查看家庭
export const getUserByFamilyId = (familyId) => {
  return axios.request({
    url: '/api/ac/school/schoolService/getUserByFamilyId?familyId=' + familyId,
    type: 'get'
  })
}

// 查询返校信息
export const getReturnSchoolList = (data) => {
  return axios.request({
    url: 'api/ac/school/returnSchoolService/getReturnSchoolList',
    type: 'post',
    data: data
  })
}

// 根据id查询返校信息
export const getReturnSchoolById = (data) => {
  return axios.request({
    url: '/api/ac/school/returnSchoolService/getApplyInfo',
    type: 'post',
    data: data
  })
}

// 学生申请提交返校申请
export const returnSchoolSave = (data) => {
  return axios.request({
    url: 'api/ac/school/returnSchoolService/StudentReturnSchool',
    type: 'post',
    data: data
  })
}

// 学生申请提交返校申请
export const returnSchoolUpdate = (data) => {
  return axios.request({
    url: 'api/ac/school/returnSchoolService/StudentRemarkAndTime',
    type: 'post',
    data: data
  })
}

// 学生返校确认
export const studentReturnInfoConfirm = (data) => {
  return axios.request({
    url: 'api/ac/school/returnSchoolService/StudentReturnInfoConfirm',
    type: 'post',
    data: data
  })
}

// 查询离校申请信息
export const getLeaveOperationList = (data) => {
  return axios.request({
    url: 'api/ac/school/leaveSchoolApply/findStudentApply',
    type: 'post',
    data: data
  })
}

// 根据id查询离校信息
export const getLeaveOperationById = (data) => {
  return axios.request({
    url: '/api/ac/school/leaveSchoolApply/getApplyInfo',
    type: 'post',
    data: data
  })
}

// 学生申请提交离校申请
export const leaveOperationSave = (data) => {
  return axios.request({
    url: 'api/ac/school/leaveSchoolApply/addLeaveSchoolApply',
    type: 'post',
    data: data
  })
}
// 学生申请更新离校申请
export const leaveOperationupdate = (data) => {
  return axios.request({
    url: 'api/ac/school/leaveSchoolApply/updateLeaveSchoolApply',
    type: 'post',
    data: data
  })
}
// 根据身份证号查看是否重复
export const checkStudentIsExist = (cardno) => {
  return axios.request({
    url: 'api/ac/school/schoolService/checkStudentIsExist?cardno=' + cardno,
    type: 'get'
  })
}

// 删除家庭里面的学生
export const deleteUser = (id) => {
  return axios.request({
    url: 'api/ac/school/schoolService/deleteUser?id=' + id,
    type: 'get'
  })
}
