import axios from '@/libs/api.request'
import config from '@/config'
/* 首页 专享套餐列表 */
export const getSdkConfig = () => {
  return axios.request({
    url: '/Api/Api/jsSdkinit/cid/' + config.company_code,
    type: 'post',
    data: {
      signurl: window.location.href
    },
    php: true
  })
}
export const getJavaSdkConfig = () => {
  return axios.request({
    url: '/api/uc/ctc/wechatService/getSignature',
    type: 'post',
    data: {
      url: window.location.href
    }
  })
}

export const getPayOptions = (order) => {
  return axios.request({
    url: '/Api/Api/justPay/',
    type: 'post',
    data: {
      signurl: window.location.href,
      extra_param: 98,
      orderid: order
    },
    php: true
  })
}
export const getMediaToOss = (media_id) => {
  return axios.request({
    url: '/Api/Api/getMediaToOss',
    type: 'post',
    data: {
      cid: config.company_code,
      media_id: media_id
    },
    php: true
  })
}

export const getUserLoginInfo = () => {
  return axios.request({
    url: '/Api/Api/getUserInfo',
    type: 'get',
    php: true
  })
}

export const getCodePic = () => {
  return axios.request({
    url: '/api/ac/duanxin/createDXPostService/getCodePic',
    type: 'get'
  })
}
/**
 * 发送验证吗
 * @param data
 * @returns {Promise<unknown> | AxiosPromise}
 */
export const createVerificationCode = (data) => {
  return axios.request({
    url: '/api/ac/duanxin/createDXPostService/createVerificationCode',
    type: 'post',
    data: data
  })
}
/**
 * 验证发送短信是否正确
 * @param data
 * @returns {Promise<unknown> | AxiosPromise}
 */
export const checkVerificationCode = (data) => {
  return axios.request({
    url: '/api/ac/duanxin/createDXPostService/checkVerificationCode',
    type: 'post',
    data: data
  })
}

// 图片上传
export const objectToOss = (data) => {
  return axios.request({
    url: '/wl/upBase64ToOss',
    data: data,
    php: true,
    // headers: { 'Content-Type': 'multipart/form-data;' },
    type: 'post'
  })
}
