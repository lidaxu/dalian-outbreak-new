import axios from '@/libs/api.request'
import config from '@/config'

// 时间转换为日期-> 2019-02-02
export function date2str (date, containtime) {
  if (!date || typeof date === 'string') {
    return date
  }
  var time1 = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  time1 += '-' + (month < 10 ? '0' + month : month)
  time1 += '-' + (day < 10 ? '0' + day : day)
  return time1
}

/* 首页 专享套餐列表 */
export const minzheng = () => {
  return axios.request({
    url: config.baseUrlMZ + '/api/......',
    type: 'get'
  })
}

export const test = () => {
  return axios.request({
    url: 'http://localhost:9501/hello/1',
    type: 'get'
  })
}
export const test2 = () => {
  return axios.request({
    url: 'http://localhost:9501/hello2/1',
    type: 'get'
  })
}

/**
 * 判断是否已经实名认证
 * @returns {Promise<unknown> | AxiosPromise}
 */
export const getByOpenid = () => {
  return axios.request({
    url: '/api/uc/ctc/identificationService/getByOpenid',
    type: 'get'
  })
}

/**
 * 传递姓名身份证号
 * @param data
 * @returns {Promise<unknown> | AxiosPromise}
 */
export const check = (data) => {
  return axios.request({
    url: '/api/uc/ctc/identificationService/check',
    type: 'post',
    data: data
  })
}
// 事项订单列表
export const orderlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditorder/orderlist',
    method: 'post',
    data: param
  })
}
// 统计每个类型的事项数量，用于tab标签页
export const tabcount = () => {
  return axios.request({
    url: '/api/as/csc/auditorder/getTabCountList',
    method: 'get'
  })
}
// 收受分离统计每个类型的事项数量，用于tab标签页
export const getTabCountSList = () => {
  return axios.request({
    url: '/api/as/csc/auditorder/getTabCountSList',
    method: 'get'
  })
}
// 已下单事项的各步骤状态变更
export const setorderapply = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditOrderService/setOrderApply',
    method: 'post',
    data: param
  })
}
// 事项id获取事项详细信息
export const iteminf = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditItemService/itemInfo',
    method: 'post',
    data: param
  })
}
// 身份证号获取用户信息
export const userinfo = (cardno) => {
  return axios.request({
    // url: '/api/as/csc/user/getuserinfobycardno',
    url: '/api/ac/csc/userRegisterService/desensitization',
    method: 'post',
    data: {
      'cardno': cardno
    }
  })
}
// 查询不同级别的通讯录列表
export const getMailLevelList = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmail/getMailLevelList',
    method: 'post',
    data: param
  })
}
// 殡葬残疾信息
export const createDisabilityApi = (param) => {
  return axios.request({
    url: '/api/ac/csc/createDisabilityOrderService/createDisabilityApi',
    method: 'post',
    data: param
  })
}
// 养老接口下拉框
export const getdmlbTreeList = (data) => {
  return axios.request({
    url: '/api/as/csc/auditYangLao/getdmlbTreeList',
    method: 'post',
    data: data
  })
}
export const noticeList = (param) => {
  return axios.request({
    url: '/api/as/csc/auditnotice/noticeList',
    method: 'post',
    data: param
  })
}
export const treelist = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditItemService/treelist',
    method: 'post',
    data: param
  })
}
export const itemInfo = (param) => {
  return axios.request({
    url: 'api/ac/csc/auditItemService/itemInfo',
    method: 'post',
    data: param
  })
}
// 创建订单信息
export const createOrder = (param) => {
  return axios.request({
    url: 'api/ac/csc/auditOrderService/createOrder',
    method: 'post',
    data: param
  })
}
// 上传表单
export const updateOrder = (param) => {
  return axios.request({
    url: 'api/ac/csc/auditOrderService/updateOrder',
    method: 'post',
    data: param
  })
}
// ocr识别
export const getOcrInfoByCard = (data) => {
  return axios.request({
    url: '/api/ac/csc/ocrService/getOcrInfoByCard',
    type: 'post',
    data: data
  })
}
// ocr识别
// export const desensitization = (data) => {
//   return axios.request({
//     url: 'api/ac/csc/userRegisterService/desensitization',
//     type: 'post',
//     data: data
//   })
// }
export const desensitization = (data) => {
  return axios.request({
    url: 'api/ac/grid/userRegisterForCSCService/desensitization',
    type: 'post',
    data: data
  })
}
// 收受分离列表
export const getSeparateList = (data) => {
  return axios.request({
    url: 'api/ac/csc/auditOrderService/getSeparateList',
    type: 'post',
    data: data
  })
}
// 收受分离列表
export const getOfficeList = (data) => {
  return axios.request({
    url: 'api/uc/csc/onSiteInspectionService/getOfficeList',
    type: 'post',
    data: data
  })
}
// 可办理的事项列表
export const eventlist = (param) => {
  return axios.request({
    url: '/api/as/csc/audititem/list',
    method: 'post',
    data: param
  })
}
export function testid (id) {
  // 1 "验证通过!", 0 //校验不通过 // id为身份证号码
  var format = /^(([1][1-5])|([2][1-3])|([3][1-7])|([4][1-6])|([5][0-4])|([6][1-5])|([7][1])|([8][1-2]))\d{4}(([1][9]\d{2})|([2]\d{3}))(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))\d{3}[0-9xX]$/
  // 号码规则校验
  if (!format.test(id)) {
    console.log('身份证号码不合规')
    this.message = '身份证号码不合规'
    return false
    // return { 'status': 0, 'msg': '身份证号码不合规' }
  }
  // 区位码校验
  // 出生年月日校验  前正则限制起始年份为1900;
  var year = id.substr(6, 4) // 身份证年
  var month = id.substr(10, 2) // 身份证月
  var date = id.substr(12, 2) // 身份证日
  var time = Date.parse(month + '-' + date + '-' + year) // 身份证日期时间戳date
  var now_time = Date.parse(new Date()) // 当前时间戳
  var dates = (new Date(year, month, 0)).getDate() // 身份证当月天数
  if (time > now_time || date > dates) {
    console.log('出生日期不合规')
    this.message = '出生日期不合规'
    return false
    // return { 'status': 0, 'msg': '出生日期不合规' }
  }
  // 校验码判断
  var c = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2] // 系数
  var b = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'] // 校验码对照表
  var id_array = id.split('')
  var sum = 0
  for (var k = 0; k < 17; k++) {
    sum += parseInt(id_array[k]) * parseInt(c[k])
  }
  if (id_array[17].toUpperCase() !== b[sum % 11].toUpperCase()) {
    console.log('身份证校验码不合规')
    this.message = '身份证校验码不合规'
    return false
    // return { 'status': 0, 'msg': '身份证校验码不合规' }
  }
  return true
  // return { 'status': 1, 'msg': '校验通过' }
}
