import axios from '@/libs/api.request'

export const userLoginMobile = (data) => { // 登录
  return axios.request({
    url: '/api/ac/transportationdalian/transportDriverServiceImpl/driverLogin',
    type: 'post',
    data: data
  })
}

// 交通转运分页列表
export const trafficTransshipList = (data) => {
  return axios.request({
    url: '/api/ac/transportationdalian/transportTaskServiceImpl/taskList',
    data: data,
    type: 'post'
  })
}

// 交通转运司机详情页
export const findTaskData = (taskId) => {
  return axios.request({
    url: '/api/ac/transportationdalian/transportTaskServiceImpl/findTaskInfo?id=' + taskId,
    type: 'get'
  })
}

// 交通转运  司机开始任务
export const startTask = (data) => {
  return axios.request({
    url: '/api/ac/transportationdalian/transportTaskServiceImpl/updateStartTask',
    data: data,
    type: 'post'
  })
}

// 交通转运  司机结束任务
export const endTask = (data) => {
  return axios.request({
    url: '/api/ac/transportationdalian/transportTaskServiceImpl/updateEndTask',
    data: data,
    type: 'post'
  })
}

// 交通转运 实时定位经纬度
export const addTaskLocationPoint = (data) => {
  return axios.request({
    url: '/api/ac/transportationdalian/transportLocationServiceImpl/addTaskLocationPoint',
    data: data,
    type: 'post'
  })
}
