/**
 * 校验身份证
 *  如果身份证末尾是以#结尾,将会把#号替换成X进行校验
 * @param {身份证号} code
 * @return 错误返回 false 正确返回 true
 */
export const IdentityCodeValid = (code) => {
  // 消除空格
  if (code) {
    code = code.toString().trim()
    // 已#号替换x
    if (code.indexOf('#') > 0) {
      code = code.replace('#', 'X')
    }
  } else {
    return false
  }

  var city = {
    11: '北京',
    12: '天津',
    13: '河北',
    14: '山西',
    15: '内蒙古',
    21: '辽宁',
    22: '吉林',
    23: '黑龙江 ',
    31: '上海',
    32: '江苏',
    33: '浙江',
    34: '安徽',
    35: '福建',
    36: '江西',
    37: '山东',
    41: '河南',
    42: '湖北 ',
    43: '湖南',
    44: '广东',
    45: '广西',
    46: '海南',
    50: '重庆',
    51: '四川',
    52: '贵州',
    53: '云南',
    54: '西藏 ',
    61: '陕西',
    62: '甘肃',
    63: '青海',
    64: '宁夏',
    65: '新疆',
    71: '台湾',
    81: '香港',
    82: '澳门',
    91: '国外 '
  }
  // var tip = ''
  var pass = true

  if (!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)) {
    tip = '身份证号格式错误'
    pass = false
  } else if (!city[code.substr(0, 2)]) {
    tip = '地址编码错误'
    pass = false
  } else {
    // 18位身份证需要验证最后一位校验位
    if (code.length === 18) {
      code = code.split('')
      // ∑(ai×Wi)(mod 11)
      // 加权因子
      var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
      // 校验位
      var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2]
      var sum = 0
      var ai = 0
      var wi = 0
      for (var i = 0; i < 17; i++) {
        ai = code[i]
        wi = factor[i]
        sum += ai * wi
      }

      var last = parity[sum % 11]
      // 身份证校验位
      var lastCode = code[17]
      if (!/^[0-9]*$/.test(last)) {
        last = last.toLowerCase()
      }
      if (!/^[0-9]*$/.test(lastCode)) {
        lastCode = lastCode.toLowerCase()
      }
      if (last !== lastCode) {
        tip = '校验位错误'
        pass = false
      }
    }
  }
  return pass
}

/**
 * 验证手机号
 * @param {需要校验的手机号} mobile
 * @return 错误返回 false , 正确返回 true
 */
export const isMobil = (mobile) => {
  var patrn = /(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/
  if (!patrn.exec(mobile)) {
    return false
  }
  return true
}

/**
 * 字符串为空校验
 * @param {需要校验的字符串} str
 * @return 为空返回 true  不为空返回 false
 */
export const isStrEmpty = (str) => {
  if (typeof (str) === 'number') {
    str = str.toString()
  }
  if (str != null && str.length > 0) {
    return false
  }
  return true
}
