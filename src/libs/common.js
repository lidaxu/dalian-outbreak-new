import { getExplorer, getDomain } from '@/libs/tools'
import { wxCallBack, wxPay, wxPayNew, share, commonShare, getLocation, openLocation, openAddress, isIos, wxUploadImg, isEmpty } from './util'
import { isMobil, isStrEmpty, IdentityCodeValid } from './validateTools'
import xeUtils from 'xe-utils'

export default {
  getExplorer: function () {
    return getExplorer()
  },
  wxCallBack,
  isMobil,
  isStrEmpty,
  isEmpty,
  IdentityCodeValid,
  xeUtils,
  getDomain,
  wxPay,
  wxPayNew,
  share,
  commonShare,
  getLocation,
  openAddress,
  isIos,
  openLocation,
  wxUploadImg
}
