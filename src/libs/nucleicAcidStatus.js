import { Toast, Dialog } from 'vant'
import router from '@/router'
import { updateSampleOpenBySampleCode } from '@/api/gov/nucleic-acid/index'

// 只提示
const sampleStatus = new Map([
  ['3', '已接收'],
  ['4', '区集中点已接收（作废）'],
  ['5', '检测点已接收（作废）'],
  ['8', '转运中'],
  ['9', '已装箱']
])
// 特殊处理 需要跳转页面 或 启瓶
const specialStatus = new Map([
  ['0', '未启用'],
  ['1', '已启用'],
  ['2', '已封瓶']
])

/**
 * // 未启用就选择5合一或者10合一然后启瓶，已启用就跳转到采集页继续采集，已封瓶就跳转到采集业只保留查看功能，其他不跳转弹提示
 * @param {*} t 状态
 * @param {*} c 样瓶编号
 * @returns
 */
const getStatus = (data) => {
  // use_state: u, sample_max_num: m
  let { sample_code: c, sample_status: t } = data
  console.log(`状态 ==> ${t}`, `样瓶编号 ==> ${c}`)
  t += ''

  console.log(sampleStatus.get(t), 'true Toast ????')
  // 判断是否只需要提示 如只需要提示 直接 提示返回
  if (sampleStatus.get(t)) return Toast.fail('此瓶' + sampleStatus.get(t))

  // 未启用就选择5合一或者10合一然后启瓶
  if (t === '0') {
    // 未启用
    Dialog.confirm({
      cancelButtonText: '5合1',
      confirmButtonText: '10合1',
      confirmButtonColor: '#323233',
      message: `<div style="font-size:0.853rem;position: relative;">此瓶未启用\n需要选择"5合1"或"10合1"进行启瓶<img src="https://ftp.bmp.ovh/imgs/2021/12/998213fc3305492a.png"  style="position: absolute;top:-1rem;right:0;width:1.2rem;height:1.2rem;"  class="re-move" /></div>`,
      closeOnClickOverlay: false // 点击遮罩层关闭弹窗
    }).then(() => { // 10合一
      console.log('选10合一')
      opener(10, data, specialStatus.get(t))
    }).catch(() => { // 5合一
      console.log('选5合一')
      opener(5, data, specialStatus.get(t))
    })
    // 因为点击遮罩层默认会选择取消的那边也就是5合1 所以在右上角加一个关闭x按钮 因为js添加的dom 需要js添加点击事件 删除弹出层dom & 黑色遮罩层
    setTimeout(() => {
      let reMove = document.querySelector('.re-move') // 右上角的x关闭按钮
      let vanDialog = document.querySelector('.van-dialog') // 弹出层dom
      let vanOverlay = document.querySelector('.van-overlay') // 黑色遮罩层dom
      reMove.onclick = function () {
        vanOverlay.remove()
        vanDialog.remove()
      }
    }, 500)
  }

  // 已启用就跳转到采集页继续采集，已封瓶就跳转到采集业只保留查看功能
  if (['1', '2'].includes(t)) {
    console.log(t, '1启用 或 2已封瓶')
    data.statusTrans = specialStatus.get(t)
    router.push({ path: '/samplingList', query: { data: JSON.stringify(data) } })
  }

  return sampleStatus.get(t)
}
/**
 *
 * @param {*} n  最大采集数
 * @param {*} c  样瓶编号
 * @param {*} t  状态
 * @param {*} tc 状态转义
 */
const opener = async (n, data, tc) => {
  const { sample_code: c } = data
  //  那个采样那个页往瓶里装人的时候五合一到三个人的时候提示：当前样瓶人数已达3人,请准备下一个样瓶!五人的时候提示：当前样瓶人数已达5人,不可再添加人员,请封瓶!然后点按钮直接封瓶，十合一的时候是8人提示：当前样瓶人数已达8人,请准备下一个样瓶!，10人的时候提示当前样瓶人数已达10人,不可再添加人员,请封瓶!然后点按钮直接封瓶
  console.log(n, data, tc, '选完5合1或10合1')
  // 启用接口
  const res = await updateSampleOpenBySampleCode({ sample_code: c, sample_max_num: n })
  if (res.data.errcode !== 0) return Toast.fail(res.data.errmsg)
  Toast.success('启用成功')
  res.data.data.statusTrans = specialStatus.get('1') // 状态转义文字 因为是刚初始化的瓶子 默认已启用
  res.data.data.use_state = 0 // 已使用的用户数 因为是刚初始化的瓶子 默认给0
  delete res.data.data._user // 删除_user 因为数据用处不大 暂时删除
  let timeout = setTimeout(() => {
    router.push({ path: '/samplingList', query: { data: JSON.stringify(res.data.data) } })
    clearTimeout(timeout)
  }, 1500)
  console.log(res, '选完5合1或10合1接口返回值')
}

export { getStatus }
