import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import common from '@/libs/common'
import config from '@/config'
import Vant from 'vant'
import 'vant/lib/index.css'
import VueWechatTitle from 'vue-wechat-title'
import VueValidator from 'vue-validator'
import './libs/rem'
import VueLazyload from 'vue-lazyload'
// import Calendar from 'vue-mobile-calendar'
import BaiduMap from 'vue-baidu-map'
Vue.use(BaiduMap, {
    // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
    ak: '7ALY8ffWj2VvrCTxhHEXp8BPbnuEGW9w'
  })
  
import VConsole from 'vconsole'
import HDSDK from 'hd-app-js-sdk'
// import { Toast } from 'vant'
import axios from 'axios'
// if (config.debug) {
//   let a = new VConsole()
//   console.log(a)
// }
// -----------------------

if (process.env.NODE_ENV !== 'production') require('@/mock')
Vue.use(Vant)
Vue.use(VueWechatTitle)
Vue.use(VueValidator)
Vue.use(VueLazyload, {
    lazyComponent: true
})

Vue.config.productionTip = false
Vue.prototype.$common = common
Vue.prototype.$config = config
router.afterEach((to, from, next) => {
        window.scrollTo(0, 0)
    })
    // Toast.loading({
    //   message: '加载中...',
    //   forbidClick: false,
    //   loadingType: 'spinner',
    //   duration: 10000
    // })
    // axios.request({ url: '/api/uc/qmc/hsMobileService/getSSTSign' }).then(r => {
    //     // Toast.clear()
    //     const result = r.data.data // 返回数据
    //     console.log(result.data, 'result111111')
    //     delete r.data.url // 删除返回数据中的url字段
    //     Vue.prototype.$hdsdk = new HDSDK(result.data) // 构造sdk实例，并挂载到vue原型
    //         // 也可以不挂载到vue原型，看项目实际需求
    // })
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')