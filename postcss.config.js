const { WIDTHVANT, WIDTHCUSTOMIZE } = require('./src/libs/rem.json')

// postcss.config.js
module.exports = ({ webpack }) => {
  let customWidthRem = WIDTHCUSTOMIZE; let dirname

  const getVantString = () => {
    dirname = JSON.parse(JSON.stringify(webpack.resourcePath))
    const dirnameNew = dirname.slice(webpack.resourcePath.indexOf('node_modules'), dirname.length)
    customWidthRem = dirnameNew.includes('vant') ? WIDTHVANT : WIDTHCUSTOMIZE
  }

  if (webpack.resourcePath.includes('node_modules')) getVantString()

  return {
    plugins: {
      'autoprefixer': {
        overrideBrowserslist: ['Android >= 4.0', 'iOS >= 7']
      },
      'postcss-pxtorem': {
        rootValue: customWidthRem,
        propList: ['*'],
        unitPrecision: 3,
        selectorBlackList: []
      }
    }
  }
}
