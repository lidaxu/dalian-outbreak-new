const path = require('path')

const resolve = dir => {
  return path.join(__dirname, dir)
}

// 项目部署基础
// 默认情况下，我们假设你的应用将被部署在域的根目录下,
// 例如：https://www.my-app.com/
// 默认：'/'
// 如果您的应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://www.foobar.com/my-app/
// 需要将它改为'/my-app/'
// iview-admin线上演示打包路径： https://file.iviewui.com/admin-dist/
const BASE_URL = process.env.NODE_ENV === 'production'
  ? '/railway'
  : '/'

module.exports = {
  // Project deployment base
  // By default we assume your app will be deployed at the root of a domain,
  // e.g. https://www.my-app.com/
  // If your app is deployed at a sub-path, you will need to specify that
  // sub-path here. For example, if your app is deployed at
  // https://www.foobar.com/my-app/
  // then change this to '/my-app/'
  publicPath: BASE_URL,
  outputDir: 'dist/railway',
  // outputDir: 'dist/citycard', // 适应张琪容器
  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  // 如果你不需要使用eslint，把lintOnSave设为false即可
  lintOnSave: true,
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set('_c', resolve('src/components'))
  },
  // 打包时不生成.map文件
  productionSourceMap: false,
  // 这里写你调用接口的基础路径，来解决跨域，如果设置了代理，那你本地开发环境的axios的baseUrl要写为 '' ，即空字符串
  // devServer: {
  //   proxy: 'localhost:3000'
  // }
  // css: {
  //   loaderOptions: {
  //     postcss: {
  //       plugins: [
  //         require('postcss-pxtorem')({
  //           rootValue: 37.5, // 换算的基数
  //           selectorBlackList: [], // 忽略转换正则匹配项
  //           propList: ['*', '!border']
  //         })
  //       ]
  //     }
  //   }
  // },
  devServer: {
    proxy: {
      '/wl/': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        //   target: 'https://dalianoutbreaknew.lngov.top',
        changeOrigin: true,
        secure: false
      },
      '/api/us/mkt/': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        //   target: 'https://dalianoutbreaknew.lngov.top',
        changeOrigin: true,
        secure: false
      },
      '/api/uc/mkt/': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        //  target: 'https://dalianoutbreaknew.lngov.top',
        changeOrigin: true,
        secure: false
      },
      '/api/uc/mobile': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        //  target: 'https://dalianoutbreaknew.lngov.top',
        changeOrigin: true,
        secure: false
      },
      '/api/': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        // target: 'http://registerdalian.lngov.top/',
        // target: 'https://dalianoutbreaknew.lngov.top',
        // target:'http://localhost:8081/',
        // target:'http://192.168.3.9:8081/',
        // target: 'http://192.168.3.85:8081/',
        // target:'http://192.168.3.95:8081/',
        // target:'http://192.168.3.95:8081/',
        changeOrigin: true,
        secure: false
      },
      '/qstbapi/': {
        ws: false,
        target: 'http://59.46.238.106:38012/',
        //   target: 'https://dalianoutbreaknew.lngov.top',
        changeOrigin: true,
        secure: false
      }
    }
  }

}
